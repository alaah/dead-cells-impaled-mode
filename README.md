![screenshot](https://steamuserimages-a.akamaihd.net/ugc/940573707168298405/E827492F1184BB80193CE4262D93DF16710F8269/)

# Howto
This is suited for [barebones](https://gitlab.com/alaah/dead-cells-barebones).

# License
See LICENSE for information.

Of course this is based on absolutely proprietary files. I consider my "changes" to be GPL. No one will care anyway.